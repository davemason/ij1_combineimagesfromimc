/*
 * Fiji Script for concatenating a folder of single-channel TIF images into a multi-channel image for VIS.
 */

#@ File (label = "Input directory", style = "directory") input


setBatchMode(true);
print("\\Clear");
close("*");
outDir=File.getParent(input);
print(outDir);print("-----------------------------");

processFolder(input);

//-- Opened all the files, so stack them together
run("Images to Stack");

//-- take the parent directory and strip the trailing slash
outfile=substring(File.directory,0, lengthOf(File.directory)-1)+".btf";
//-- for import into VIS save as BTF, note that regular TIF does not provide pyramidal scaling and messes up the input calibration
run("OME-TIFF...", "save=["+outfile+"] export compression=Uncompressed");

//-- save out the channel log
selectWindow("Log");
saveAs("Text", replace(outfile,".btf",".txt"));
close("Log");

//-- If you want to see the stack, uncomment the following line:
//setBatchMode("exit and display");

//////////////////////////////////////////////////////////////////
//-- Functions
//////////////////////////////////////////////////////////////////

// function to scan folders/subfolders/files to find files with correct suffix
function processFolder(input) {
	list = getFileList(input);
	list = Array.sort(list);
	//Array.print(list);
	
	for (i = 0; i < list.length; i++) {
		processFile(input, list[i]);
	}
}

function processFile(input,file) {

	if (endsWith(file, ".tiff")){
		print(file);
		//-- Depending upon how the file is exported originally, regular Fiji open may not pull the correct calibration
		//open(input + File.separator + file);
		run("Bio-Formats Importer", "open=["+input + File.separator + file+"] color_mode=Default rois_import=[ROI manager] view=Hyperstack stack_order=XYCZT");
		title=getTitle();
		run("Select None");
		run("Set Label...", title);
	}
}